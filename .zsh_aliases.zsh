# ----------------------
# Z Shell
# ----------------------
alias reload="source ~/.zshrc"

# ----------------------
# GIT
# ----------------------
alias gs='git status'
alias gl='git log'
alias gaa='git add .'
alias gc="git commit"
alias gcfl="git commit -m 'WIP: For later'"
alias gamend="git commit --amend"
alias gp="git pull"
alias gpr="git pull --rebase"
alias gcpr="git push origin HEAD:refs/for/master"
alias gr1="git reset --soft HEAD~1"
alias gpu="git push"
