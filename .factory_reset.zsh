#!/bin/bash

echo ".centralized-macos-config: Resetting configurations to factory defaults"

rm -rf ~/.zshrc ~/.zshrc_before_centralized_macos_config ~/.oh-my-zsh ~/.config