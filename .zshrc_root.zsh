# Linking to centralized configuration
if [ -f ~/.centralized-macos-config/.zshrc ]; then
    source ~/.centralized-macos-config/.zshrc
fi

# Linking to machine specific aliases
if [ -f ~/.custom_aliases.zsh ]; then
    . ~/.custom_aliases.zsh
fi

# Linking to machine specific .zshrc
if [ -f ~/.custom_zshrc.zsh ]; then
    . ~/.custom_zshrc.zsh
fi