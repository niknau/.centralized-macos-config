#.centralized-macos-config

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Motivation](#motivation)
- [Getting Started](#getting-started)
- [Keeping In Sync](#keeping-in-sync)
- [Are you not entertained?](#are-you-not-entertained)
- [Troubleshooting](#troubleshooting)

## Motivation

Configuring and maintaining configurations between machines can be a hassle. This tool aims at making life easier when changing configurations (e.g. iTerm conf) and wanting to sync these between macOS devices. It contains three functions, `install`, `sync`, and `factory reset`.

## Getting Started

    git clone https://niknau@bitbucket.org/niknau/.centralized-macos-config.git && cd .centralized-macos-config/ && zsh .install.zsh

This script will install devtools and programs commonly used:

- [Homebrew](https://brew.sh/)
- [iTerm2](https://iterm2.com/)
  - [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh)
  - [powerlevel10k](https://github.com/romkatv/powerlevel10k)
  - [neofetch](https://github.com/dylanaraps/neofetch)
- [VS Code](https://code.visualstudio.com/)
- [Slack](https://slack.com/)
- [Postman](https://www.postman.com/)
- [Docker](https://www.docker.com/)

:white_check_mark: You are now ready to go. To make sure everything has been set up correctly, open up iTerm2 and make sure you've got an awesome looking terminal. If something is missing or looks incorrect, please head over to the

## Keeping In Sync

Located at `~./.centralized-macos-config`, run `git pull` followed by `zsh sync.zsh`.

## Are you not entertained?

Located at `~./.centralized-macos-config`, restore to macOS default configurations by running `zsh factory_reset.zsh`.

## Troubleshooting

- ##### No changes have been made after running the installer
  1. Try `⌘`+`Q` to quit iTerm2 and start it again
  2. If the above does not work, navigate to `~./.centralized-macos-config` in iTerm and run `zsh sync.zsh`. Then perform the first step again
   
- ##### Icons are displayed as question marks

  Navigate to `iTerm2 > Preferences > Profile > Text > Font` and choose your `Meslo Nerd Font` of choice
  Make sure your VS Code settings.json specifies the Meslo Nerd Font:
    ```
    {
        "terminal.integrated.fontFamily": "MesloLGS NF"
    }
- ##### My iTerm2 color scheme is not what I expected
      Navigate to `iTerm2 > Preferences > Profile > Colors > Color Presets > Import`
  Now import the `flat-colors.itermcolors` preset located at root in this project and activate it.

<!-- #### Step by step

##### 1. If you haven't already, go ahead and install :

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" echo "export PATH=/opt/homebrew/bin:$PATH" > .zshrc

##### 2. Install [iTerm2](https://iterm2.com/):

    brew install --cask iterm2

##### 3. Install [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh):

    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

##### 4. Install [powerlevel10k](https://github.com/romkatv/powerlevel10k):

    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

##### 5. Install [neofetch](https://github.com/dylanaraps/neofetch):

    brew install neofetch

##### 6. Install [VS Code](https://code.visualstudio.com/):

    brew install --cask visual-studio-code

##### 7. Create symlink to our centralized and version controlled configurations

Located at your `$HOME` path, open up your terminal.

All in one step:

    cp .zshrc .zshrc_before_centralized_macos_config && rm -rf .zshrc && echo "if [ -f ~/.centralized-macos-config/.zshrc ]; then source ~/.centralized-macos-config/.zshrc fi" > .zshrc

Or step by step:

1.  Let us create a backup of your current Z Shell configuration
    `cp .zshrc .zshrc_before_centralized_macos_config`
2.  Now that we have a backup for it, lets tweak it a bit:
    `code .zshrc`
3.  Replace the existing content with
    <code>echo "if [ -f ~/.centralized-macos-config/.zshrc ]; then source ~/.centralized-macos-config/.zshrc fi" > .zshrc</code>.
    This will point your machine specific Z Shell configuration to the centralized one.

##### 8. Set iTerm color profile

Navigate to `iTerm2 > Preferences > Profile > Colors > Color Presets > Import`
Now import the `flat-colors.itermcolors` preset located at root in this project and activate it.

##### 9. Set iTerm font family

Navigate to `iTerm2 > Preferences > Profile > Text > Font` and choose your `Meslo Nerd Font` of choice. -->
