#!/bin/bash

# Function to allow for iTerm color profile configuration programmatically
it2prof() { echo -e "\033]50;SetProfile=$1\a" }

NO_COLOR='\033[0;m' # No Color
echo_success() {
    CHECK_MARK="\033[1;32m✓${NO_COLOR}"
    echo -e "${CHECK_MARK} $1"
}

###########################################
#### Backing up previous configurations ###
###########################################
if [ -f ~/.zshrc ]; then
    cp ~/.zshrc ~/.zshrc_before_centralized_macos_config_sync && echo_success "Backed up current Z Shell configuration in ~/.zshrc_before_centralized_macos_config_sync"
fi
if [ -d ~/.config ]; then
    rm -rf ~/config_before_centralized_macos_config_sync
    mkdir ~/config_before_centralized_macos_config_sync && cp -R ~/.config ~/.config_before_centralized_macos_config_sync && echo_success "Backed up current .config folder in ~/.config_before_centralized_macos_config_sync"
fi

###########################################
############## Configurating ##############
###########################################

# Z Shell
rm -rf ~/.zshrc
cp ./.zshrc_root.zsh ~/.zshrc && echo_success "Configurated Z Shell"

# iTerm and neofetch config
rm -rf ~/.config
cp -R ./.config ~/.config && echo_success "Configurated iTerm" && echo_success "Configurated neofetch"

# Fonts
cp ./fonts/* ~/Library/Fonts/ && echo_success "Configurated fonts"

# Configuring aliases
rm -rf ~/.oh-my-zsh/custom/aliases.zsh
cp ./.zsh_aliases.zsh ~/.oh-my-zsh/custom/aliases.zsh && echo_success "Configurated aliases"
