#!/bin/bash

NO_COLOR='\033[0;m' # No Color
echo_success() {
    CHECK_MARK="\033[1;32m✓${NO_COLOR}"
    echo -e "${CHECK_MARK} $1"
}

brew_install() {
    echo "\nInstalling $1"
    if brew list $1 &>/dev/null; then
        echo "${1} is already installed"
    else
        brew install $1 && echo "$1 is installed"
    fi
}

brew_cask_install() {
    echo "\nInstalling $1"
    if brew --cask list $1 &>/dev/null; then
        echo "${1} is already installed"
    else
        brew install --cask $1 && echo "$1 is installed"
    fi
}

echo ".centralized-macos-config: Installing prerequisites"

# Homebrew
if test ! $(which brew); then
    echo "Installing homebrew..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" echo "export PATH=/opt/homebrew/bin:$PATH" > ~/.zshrc
fi
PATH=/opt/homebrew/bin:$PATH

# iTerm2
brew_cask_install "iterm2"

# neofetch
brew_install "neofetch" &

# VS Code
brew_cask_install "visual-studio-code" &

# Slack
brew_cask_install "slack"

# Postman
brew_cask_install "postman"

# Docker
brew_install "docker"

# XCode
brew_install "cocoapods"

# oh-my-zsh
{ cd ~ && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh) --unattended"; cd -; } &&

# powerlevel10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/custom/themes/powerlevel10k

# zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions

# zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

echo_success "Installation finished"
echo "Syncing configurations"

zsh .sync.zsh